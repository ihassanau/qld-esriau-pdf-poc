var app;
var arr = [];
//var ctx;
var canvas;
var img;
var d;
function copyDIV() {
  if (d) {
    document.body.removeChild(d);
  }


  d = document.getElementById('the_div').cloneNode(true);
  d['id'] = 'new_div'
  document.body.appendChild(d);
  do_the_magic();
};


function do_the_magic() {
  var rotation_value = $('#new_div').css('transform');

  $('#new_div').css('transform', 'matrix(1, 0, 0, 1, 0, 0)')
  var elem = $('#new_div svg')[0];
  $(elem).css('transform', 'matrix(1, 0, 0, 1, 0, 0)');
  var xml = new XMLSerializer().serializeToString(elem);
  var svg64 = btoa(xml);
  var b64Start = 'data:image/svg+xml;base64,';

  // prepend a "header"
  var image64 = b64Start + svg64;

  // set it as the source of the img element
  var old_img;
  if (img) {
    old_img = img;
  }

  img = new Image();
  img.src = image64;
  img.id = 'img-' + arr.length;
  arr.push(img);
  if (old_img) {
    $(old_img).css('display', 'none');
    document.body.insertBefore(img, old_img);
  }
  else
    document.body.appendChild(img);


    function imgLoad(){
      var canvas = document.getElementById("map-export-canvas");
      canvas.width = this.width;
      canvas.height = this.height;
      
      var ctx = canvas.getContext('2d');
      
  
      
  
      ctx.drawImage(this, 0, 0, this.width, this.height, 0, 0, this.width, this.height);
      console.debug(this.src);
    };

  img.onload = imgLoad;
};

function copy() {
  var i = 0;
  var arr_viewLayers = $('svg').each(function (index, elem) {
    if ($(elem).parent().parent()[0].className == 'esri-view-surface') {
      return;
    }
    $(elem).parent()[0]['id'] = 'the_div';
    // var xml = new XMLSerializer().serializeToString(elem);
    // var svg64 = btoa(xml);
    // var b64Start = 'data:image/svg+xml;base64,';

    // // prepend a "header"
    // var image64 = b64Start + svg64;

    // // set it as the source of the img element
    // var old_img;
    // if(img){
    //   old_img = img;
    // }
    // img = new Image();
    // img.src = image64;
    // img.id = 'img-'+arr.length;
    // arr.push(img);

    // // if(old_img)
    // //   document.body.insertBefore(img, old_img);
    // // else
    // //   document.body.appendChild(img);
    // img.onload = imgLoad;



  });





};

require([
  "esri/config",
  "esri/Map",
  "esri/views/MapView",
  "esri/layers/WMSLayer",
  "esri/layers/MapImageLayer",
  "esri/widgets/Legend",
  'esri/geometry/ScreenPoint',
  'esri/layers/FeatureLayer',
  "dojo/domReady!"
], function (
  esriConfig,
  Map,
  MapView,
  WMSLayer,
  MapImageLayer,
  Legend,
  ScreenPoint,
  FeatureLayer
) {

    // Use a proxy for the initial WMS GetCapabilities request if the WMS server is not enabled for CORS
    esriConfig.request.proxyUrl = "http://localhost/DotNet/proxy.ashx";

    esriConfig.request.corsEnabledServers.push("http://data.gov.au");
    esriConfig.request.corsEnabledServers.push("http://services.ga.gov.au");
    var labels_layer = new MapImageLayer({
      url: "https://gisservices4.information.qld.gov.au/arcgis/rest/services/Location/Places/MapServer"
    });
    var layer = new WMSLayer({
      //url: "http://data.gov.au/geoserver/native-title-determination-outcomes/wms"
      url: "http://services.ga.gov.au/site_9/services/CWTH_CW_Act_1980_Coastal_Waters_AMB2014a/MapServer/WMSServer"
    });

    var fl = new FeatureLayer({
      url: "https://sampleserver6.arcgisonline.com/arcgis/rest/services/Census/MapServer/3"
    });

    var map = new Map({
      basemap: "gray-vector",
      layers: [fl]

    });

    var view = new MapView({
      container: "viewDiv",
      map: map,
      center: [-106.35226, 37.6143483],
      zoom: 3
    });

    layer.load().then(function () {
      //console.debug(tile_layer);
      //view.extent = layer.fullExtent;
      //view.map = map;
    });

    document.getElementById('viewDiv').onmousemove = function (evt) {
      var screenPoint = new ScreenPoint({ x: evt.offsetX, y: evt.offsetY });
      var point = view.toMap(screenPoint);
      document.getElementById('screenPoint').innerText = "x: " + screenPoint.x + " --- y: " + screenPoint.y;

    };

    app = {
      map: map,
      mapView: view
    }






  });