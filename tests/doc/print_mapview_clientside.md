# Print Map View - Client Side Image

Print 2D map using an image generated client side and converted to PDF on the server.

## Overview

When the user clicks the print button in the  qld globe a new browser tab opens displaying a map, a selection of templates and print options. 

If the user selects to print a 2D (MapView) and presses the print button, an image is generated client side from each of the layers in the MapView and rendered into a html page. This html page is then sent to the server for conversion to PDF.

The generation of the map image is not out of the box. It relies on various unpublished hooks into the MapView to allow images of the different layers to be generated and then composited together on a html canvas for exporting.

The user is informed of the progress while the print is being generated.

### Risks



## Method

## 