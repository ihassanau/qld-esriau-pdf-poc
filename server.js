var express    = require('express');
var serveIndex = require('serve-index');

var app = express();
var port = 9021;

// Serve URLs like /ftp/thing as public/ftp/thing
// The express.static serves the file contents
// The serveIndex is this module serving the directory
app.use('', express.static('tests'), serveIndex('tests', {'icons': true}));

// Listen
app.listen(port);

console.log("Magic happens on port " + port);
